#include <wx/wx.h>
#include "Setup.h"
#include "CMain.h"

#include "Server.h"
#include "Client.h"


class CApp : public wxApp
{
public:
	CApp();

	~CApp();

	virtual bool OnInit();

	void switchDisplay();

	void startServer(std::string rawIp, unsigned short portNum);

	void startClient(std::string rawIp, unsigned short portNum);

	std::string getUsername();

	void setUserName(std::string& username);

	void sendMessage(Message m);

	

private:	

	CMain* _frame1 = nullptr;
	Setup* _setupScreen = nullptr;

	Server* _server = nullptr;
	Client* _client = nullptr;

	std::string _username;

	bool _isServer = false;

	boost::asio::io_context context;

	std::jthread _contextThread;
	std::jthread _serverThread;
};


