#include "Client.h"

Client::Client(boost::asio::io_context& context)
	:_context(context)
{

}

void Client::connect(std::string& rawIp, unsigned short portNum) {
	try {
		boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string(rawIp), portNum);

		boost::asio::ip::tcp::socket sock(_context, ep.protocol());

		sock.connect(ep);

		if (sock.is_open()) {
			_connection = std::make_shared<Connection>(sock, _inQueue, 0);
			_connection->setVerified(true);
			_connection->start();
		}

	}
	catch (boost::system::system_error& ec) {
		std::cout << ec.what() << std::endl;
	}
}

void Client::send(Message& m) {
	_connection->send(m);
}

void Client::update() {
	_inQueue.wait();

	while (!_inQueue.empty()) {
		Message m = _inQueue.pop_front();

		onMessage(m);
	}
}

void Client::onMessage(Message& m) {
	if (m.getHeader().mesageType == MessageType::TEXT) {
		_textCtrl->AppendString(Message::extractTextMessage(m));
	}
	else if (m.getHeader().mesageType == MessageType::VERIFY_CLIENT) {
		verifyWithServer(m);
	}
	else if (m.getHeader().mesageType == MessageType::ACCEPT) {
		std::cout << "[The server has accepted your connection!]" << std::endl;
		std::cout << "You are inside the chatroom now" << std::endl;
	}
}


void Client::verifyWithServer(Message& m) {
	int numFromServer;

	m >> numFromServer;

	int answer = numFromServer * 3;

	Message verification;
	verification.getHeader().mesageType = MessageType::VERIFY_WITH_SERVER;
	verification << numFromServer << answer;

	send(verification);
}
