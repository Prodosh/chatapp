#pragma once
#include "Message.h"

#include <deque>

class TSQueue
{
public:
	TSQueue();

	void push_back(Message& m);

	Message& front();

	Message pop_front();


	bool empty();

	void wait();

	int getSize();

private:
	int size = 0;
	std::deque<Message> _messageQ;
	std::mutex _mutex;
	std::mutex waitMux;
	std::condition_variable blocking;

};

