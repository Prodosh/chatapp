#include "TSQueue.h"

TSQueue::TSQueue() {

}

void TSQueue::push_back(Message& m) {
	std::lock_guard<std::mutex> guard(_mutex);
	_messageQ.push_back(m);
	blocking.notify_one();
	++size;
}

Message& TSQueue::front() {
	std::lock_guard<std::mutex> guard(_mutex);

	return _messageQ.front();
}

Message TSQueue::pop_front() {
	std::lock_guard<std::mutex> guard(_mutex);

	Message m = std::move(_messageQ.front());
	_messageQ.pop_front();
	--size;

	return m;
}

bool TSQueue::empty() {
	std::lock_guard<std::mutex> guard(_mutex);
	return _messageQ.empty();
}

void TSQueue::wait() {
	while (empty()) {
		std::unique_lock<std::mutex> ul(waitMux);
		blocking.wait(ul);
	}
}

int TSQueue::getSize() {
	std::lock_guard<std::mutex> guard(_mutex);
	return _messageQ.size();
}