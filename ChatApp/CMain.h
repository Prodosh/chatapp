#pragma once

#include <wx/wx.h>
class CApp;
class CMain : public wxFrame
{
public:
	CMain(CApp* parent);
	~CMain();

	CApp* _parent = nullptr;

	wxButton* _sendBtn = nullptr;
	wxTextCtrl* _textInput = nullptr;
	wxListBox* _messageDisplay = nullptr;

	void onButtonClicked(wxCommandEvent& evt);

	wxDECLARE_EVENT_TABLE();
};

