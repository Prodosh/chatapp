#include "CApp.h"

wxIMPLEMENT_APP(CApp);

CApp::CApp()
{
}

CApp::~CApp()
{
    delete _server;
    delete _client;
}

bool CApp::OnInit()
{
    _frame1 = new CMain(this);
    _setupScreen = new Setup(this);
    _setupScreen->Show();
    return true;
}

void CApp::switchDisplay()
{
    _setupScreen->Hide();
    _frame1->Show();
}

void CApp::startServer(std::string rawIp, unsigned short portNum)
{
    _server = new Server(context);
	_server->_textCtrl = _frame1->_messageDisplay;
	_isServer = true;
	try {
		_server->start(rawIp, portNum);
	}
	catch (boost::system::system_error& ec) {
		std::cout << ec.what() << std::endl;
	}

	_contextThread = std::jthread([&]() {
		context.run();
	});

	_serverThread = std::jthread([&]() {
		while (true) {
			_server->update();
		}
	});
}

void CApp::startClient(std::string rawIp, unsigned short portNum)
{
	_client = new Client(context);
	_client->_textCtrl = _frame1->_messageDisplay;
	_isServer = false;
	_client->connect(rawIp, portNum);

	_contextThread = std::jthread([&]() {
		context.run();
	});

	_serverThread = std::jthread([&]() {
		while (true) {
			_client->update();
		}
	});
}

std::string CApp::getUsername()
{
	return _username;
}

void CApp::setUserName(std::string& username)
{
	_username = username;
}

void CApp::sendMessage(Message m)
{
	if (m.getHeader().mesageType == MessageType::TEXT) {
		_frame1->_messageDisplay->AppendString(Message::extractTextMessage(m));
	}

	if (_isServer) {
		_server->addMessageToLog(m);
		_server->messageAll(m, 0);
	}
	else {
		_client->send(m);
	}
}
