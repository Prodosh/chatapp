#include "CMain.h"
#include "CApp.h"

wxBEGIN_EVENT_TABLE(CMain, wxFrame)
	EVT_BUTTON(10003, onButtonClicked)
wxEND_EVENT_TABLE()

CMain::CMain(CApp* parent) : wxFrame(nullptr, wxID_ANY, "Chat Application", wxPoint(30, 30), wxSize(350, 600))
{
	_parent = parent;
	_messageDisplay = new wxListBox(this, wxID_ANY, wxPoint(10, 10), wxSize(300, 300));	
	_textInput = new wxTextCtrl(this, wxID_ANY, "", wxPoint(10, 320), wxSize(300, 30));
	_sendBtn = new wxButton(this, 10003, "Send", wxPoint(10, 360), wxSize(150, 50));
	
}


CMain::~CMain()
{
}

void CMain::onButtonClicked(wxCommandEvent& evt)
{
	std::string stringMessage = _textInput->GetValue().ToStdString();

	size_t usernameLength = _parent->getUsername().length() <= 15 ? _parent->getUsername().length() : 15;

	char text[200];

	char usernameArray[15];

	memcpy(usernameArray, _parent->getUsername().data(), usernameLength);

	if (!stringMessage.empty() && stringMessage.length() < 200)
	{
		memcpy(text, stringMessage.data(), stringMessage.length());

		time_t now = time(0);

		Message m;
		m.getHeader().mesageType = MessageType::TEXT;
		m.getHeader().timeSent = now;
		m << usernameArray << usernameLength << text << stringMessage.length();
		
		_parent->sendMessage(m);
	}

	_textInput->Clear();
	evt.Skip();
}

