#include "MessageDisplayer.h"


MessageDisplayer::MessageDisplayer()
{
}

MessageDisplayer::MessageDisplayer(wxListBox* messageDisplay)
	: _messageDisplay(messageDisplay)
{
}

void MessageDisplayer::displayMessage(Message& m)
{
	if (_messageDisplay != nullptr) {
		tm* ltm = localtime(&m.getHeader().timeSent);



		char username[15];
		size_t usernameLength;
		char text[200];
		size_t length;

		m >> length >> text >> usernameLength >> username;

		std::string usernameString(username);

		std::string message(text);

		//std::string timeString = ltm->tm_mday + "/" + 1 + ltm->tm_mon + "/" + 1900 + ltm->tm_year + " " + 5 + ltm->tm_hour + ":" + 30 + ltm->tm_min + ":" + ltm->tm_sec;

		_messageDisplay->AppendString(usernameString + "         " + message);
	}
}

void MessageDisplayer::setMessageListBox(wxListBox* messageDisplay)
{
	_messageDisplay = messageDisplay;
}
