#pragma once
#include <wx/wx.h>
#include <vector>
#include <cstdlib>

#include "Connection.h"

class Server {
public:

	Server(boost::asio::io_context& context);


	~Server();

public:
	
	void onMessage(Message& m);

	void start(std::string& rawIp, unsigned short portNum);

	void update();

	void addMessageToLog(Message& m);

	void messageAll(Message& m, int clientID);

	wxListBox* _textCtrl = nullptr;

private:	

	void acceptConnections();

	void sendClientVerification(std::shared_ptr<Connection> c);

	void verifyClient(Message& m);

	void rejectClient(int clientID);

	void acceptClient(int clientID);

	void shareMessageLog(std::shared_ptr<Connection> con);

	

private:
	boost::asio::ip::tcp::acceptor* _acceptor;
	boost::asio::io_context& _context;
	std::vector<std::shared_ptr<Connection>> _connections;
	std::vector<Message> _messageLog;
	TSQueue _inQueue;
	int _connectionID = 1;

	
};

