#pragma once

#include "MessageTypes.h"
#include <chrono>

struct MessageHeader {
	time_t timeSent;
	int senderID = 0;
	MessageType mesageType;
	size_t _size = 0;
};

class Message
{
public:
	Message() {
		_data.resize(0);
	}

	template <typename T>
	Message& operator<<(const T& d) {
		static_assert(std::is_standard_layout<T>::value, "Data is too complex");

		_data.resize(_data.size() + sizeof(T));

		memcpy(_data.data() + getSize(), &d, sizeof(T));

		_header._size += sizeof(T);

		return *this;
	}

	template <typename T>
	Message& operator>>(T& d) {
		static_assert(std::is_standard_layout<T>::value, "Data is too complex");
		memcpy(&d, _data.data() + (_data.size() - sizeof(T)), sizeof(T));

		_data.resize(_data.size() - sizeof(T));

		_header._size -= sizeof(T);

		return *this;
	}


	size_t getSize() {
		return _header._size;
	}

	MessageHeader& getHeader() {
		return _header;
	}

	std::vector<uint8_t> getData() {
		return _data;
	}
	std::vector<uint8_t> _data;

	MessageHeader _header;

	static std::string extractTextMessage(Message m) {
		tm* ltm = localtime(&m.getHeader().timeSent);

		std::string timeString = std::to_string(ltm->tm_mday) + "/" + std::to_string(1 + ltm->tm_mon) + "/" + std::to_string(1900 + ltm->tm_year) + " " + std::to_string(5 + ltm->tm_hour) + ":" + std::to_string(30 + ltm->tm_min) + ":" + std::to_string(ltm->tm_sec);
		char username[15];
		size_t usernameLength;
		char text[200];
		size_t length;

		m >> length >> text >> usernameLength >> username;

		std::string usernameString;
		usernameString.resize(usernameLength);
		memcpy(usernameString.data(), username, usernameLength);

		std::string message;
		message.resize(length);
		memcpy(message.data(), text, length);

		std::string displayString =  timeString + "\n " + usernameString + ": " + message;

		return displayString;
	}

};

