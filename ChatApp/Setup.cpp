#include "Setup.h"
#include "CApp.h"

wxBEGIN_EVENT_TABLE(Setup, wxFrame)
	EVT_BUTTON(10001, onStartServerClicked)
	EVT_BUTTON(10002, onStartClientClicked)
wxEND_EVENT_TABLE()

Setup::Setup(CApp* parent)
	: wxFrame(nullptr, wxID_ANY, "Setup", wxPoint(30, 30), wxSize(550, 350))
{
	_parent = parent;

	_usernameLabel = new wxStaticText(this, wxID_ANY, "Enter a username:", wxPoint(50, 60));
	_usernameInput = new wxTextCtrl(this, wxID_ANY, "", wxPoint(50, 75), wxSize(450, 25));

	_ipLabel = new wxStaticText(this, wxID_ANY, "Enter an IP address:", wxPoint(50, 135));
	_ipInput = new wxTextCtrl(this, wxID_ANY, "", wxPoint(50, 150), wxSize(450, 25));

	_portNumLLabel = new wxStaticText(this, wxID_ANY, "Enter a port number:", wxPoint(50, 210));
	_portInput = new wxTextCtrl(this, wxID_ANY, "", wxPoint(50, 225), wxSize(450, 25));

	_startServer = new wxButton(this, 10001, "Start Server", wxPoint(50, 270), wxSize(100, 30));
	_connectToServer = new wxButton(this, 10002, "Connect to Server", wxPoint(200, 270), wxSize(100, 30));
}

Setup::~Setup()
{
}

void Setup::onStartServerClicked(wxCommandEvent& evt)
{
	std::string userName = _usernameInput->GetValue().ToStdString();
	_parent->setUserName(userName);
	unsigned short portNum = (unsigned short)std::strtoul(_portInput->GetValue().c_str(), NULL, 0);
	_parent->startServer(_ipInput->GetValue().ToStdString(), portNum);

	_parent->switchDisplay();
	evt.Skip();
}

void Setup::onStartClientClicked(wxCommandEvent& evt)
{
	std::string userName = _usernameInput->GetValue().ToStdString();
	_parent->setUserName(userName);
	unsigned short portNum = (unsigned short)std::strtoul(_portInput->GetValue().c_str(), NULL, 0);

	_parent->startClient(_ipInput->GetValue().ToStdString(), portNum);

	_parent->switchDisplay();
	evt.Skip();
}
