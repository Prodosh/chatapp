#pragma once
#include "wx/wx.h"
#include "Message.h"
#include <ctime>

class MessageDisplayer
{
public:
	MessageDisplayer();
	MessageDisplayer(wxListBox* messageDisplay);

	void displayMessage(Message& m);

	void setMessageListBox(wxListBox* messageDisplay);

private:

	wxListBox* _messageDisplay = nullptr;
};

