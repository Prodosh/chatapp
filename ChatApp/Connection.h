#pragma once

#include <memory>
#include <iostream>

#include "TSQueue.h"
#include "boost/asio.hpp"


class Connection : public std::enable_shared_from_this<Connection>
{
public:
	Connection(boost::asio::ip::tcp::socket& sock, TSQueue& inMessageQueue, int id);

	void send(Message& m);

	bool isOpen();

	void close();

	void start();

	int getID();

	bool getVerified();

	void setVerified(bool v);

public:
	Message _tempMessage;

private:

	void writeHeader();

	void writeBody();

	void readHeader();

	void readBody(size_t bodySize);	

private:
	boost::asio::ip::tcp::socket _sock;
	TSQueue _outMessageQueue;
	TSQueue& _inMessageQueue;
	int _id = 0;
	bool verified = false;
};

