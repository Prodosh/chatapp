#include "Connection.h"

Connection::Connection(boost::asio::ip::tcp::socket& sock, TSQueue& inMessageQueue, int id)
	: _sock(std::move(sock)), _inMessageQueue(inMessageQueue), _id(id) {
}

void Connection::writeHeader() {
	boost::asio::async_write(_sock, boost::asio::buffer(&_outMessageQueue.front().getHeader(), sizeof(MessageHeader)), [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
		if (!ec) {
			//std::cout << "Trasnfered header " << bytes_transferred << " bytes" << std::endl;

			writeBody();
		}
		else
		{
			std::cout << ec.message() << std::endl;
		}
	});
}

void Connection::writeBody() {
	boost::asio::async_write(_sock, boost::asio::buffer(_outMessageQueue.front().getData().data(), _outMessageQueue.front().getSize()), [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
		if (!ec) {
			//std::cout << "Trasnfered body " << bytes_transferred << " bytes" << std::endl;

			_outMessageQueue.pop_front();
		}
		else {
			std::cout << ec.message() << std::endl;
		}

		if (_outMessageQueue.getSize() > 0) {
			writeHeader();
		}
	});
}

void Connection::readHeader() {
	boost::asio::async_read(_sock, boost::asio::buffer(&_tempMessage.getHeader(), sizeof(MessageHeader)), [this](const boost::system::error_code& ec, std::size_t bytes_transfered) {
		if (!ec) {
			//std::cout << "reading header: " << bytes_transfered << " bytes recieved" << std::endl;
			//std::cout << "Size is " << _tempMessage.getHeader()._size << std::endl;			

			_tempMessage.getHeader().senderID = this->getID();
			readBody(_tempMessage.getHeader()._size);


		}
		else {
			readHeader();
		}

	});
}

void Connection::readBody(size_t bodySize) {
	_tempMessage._data.resize(bodySize);
	boost::asio::async_read(_sock, boost::asio::buffer(_tempMessage._data.data(), bodySize), [this](const boost::system::error_code& ec, std::size_t bytes_transfered) {
		if (!ec) {
			//std::cout << "reading body " << bytes_transfered << " bytes recieved" << std::endl;
			if (verified || _tempMessage.getHeader().mesageType == MessageType::VERIFY_WITH_SERVER)
			{
				_inMessageQueue.push_back(_tempMessage);
			}
			else
			{
				Message unauth;
				unauth.getHeader().mesageType = MessageType::UNAUTHORIZED;
				unauth.getHeader().senderID = this->getID();
				_inMessageQueue.push_back(unauth);
			}
		}
		else
		{
			std::cout << ec.message() << std::endl;
		}

		readHeader();
		});
}

void Connection::send(Message& m) {
	int size = _outMessageQueue.getSize();

	_outMessageQueue.push_back(m);

	if (size == 0) {
		writeHeader();
	}
}

bool Connection::isOpen() {
	return _sock.is_open();
}

void Connection::close() {
	_sock.close();
}

void Connection::start() {
	readHeader();
}

int Connection::getID() {
	return _id;
}

bool Connection::getVerified()
{
	return verified;
}

void Connection::setVerified(bool v) {
	verified = v;
}