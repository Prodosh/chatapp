#pragma once
#include <wx/wx.h>
#include "Connection.h"



class Client {
public:
	Client(boost::asio::io_context& context);

	void connect(std::string& rawIp, unsigned short portNum);

	void send(Message& m);

	void update();

	void onMessage(Message& m);

	wxListBox* _textCtrl = nullptr;

private:

	void verifyWithServer(Message& m);

	boost::asio::io_context& _context;
	std::shared_ptr<Connection> _connection;
	TSQueue _inQueue;
};

