#pragma once

#include <wx/wx.h>

class CApp;

class Setup : public wxFrame
{
public:
	Setup(CApp* parent);
	~Setup();

	wxStaticText* _usernameLabel = nullptr;
	wxStaticText* _ipLabel = nullptr;
	wxStaticText* _portNumLLabel = nullptr;
	wxTextCtrl* _usernameInput = nullptr;
	wxTextCtrl* _ipInput = nullptr;
	wxTextCtrl* _portInput = nullptr;
	wxButton* _startServer = nullptr;
	wxButton* _connectToServer = nullptr;

	CApp* _parent = nullptr;



	void onStartServerClicked(wxCommandEvent& evt);
	void onStartClientClicked(wxCommandEvent& evt);

	wxDECLARE_EVENT_TABLE();
};

