#include "Server.h"

Server::Server(boost::asio::io_context& context)
	:_context(context){


}


Server::~Server() {
	delete _acceptor;
}

void Server::shareMessageLog(std::shared_ptr<Connection> con) {
	for (auto& message : _messageLog) {
		con->send(message);
	}
}

void Server::acceptConnections() {
	_acceptor->async_accept([this](const boost::system::error_code& ec, boost::asio::ip::tcp::socket peer) {
		if (!ec) {
			std::cout << "connection recieved" << std::endl;
			boost::asio::ip::tcp::endpoint remote = peer.remote_endpoint();

			std::shared_ptr<Connection> c = std::make_shared<Connection>(peer, _inQueue, ++_connectionID);
			_connections.push_back(c);
			c->start();
			sendClientVerification(c);
		}
		else
		{
			std::cout << ec.message() << std::endl;
		}
		acceptConnections();
		});

}

void Server::sendClientVerification(std::shared_ptr<Connection> c) {
	srand((unsigned)time(NULL));
	int offset = 1;
	int range = 100;
	int randomNum = offset + (rand() & range);

	Message m;
	m.getHeader().mesageType = MessageType::VERIFY_CLIENT;
	m << randomNum;
	c->send(m);
}

void Server::verifyClient(Message& m) {
	int answer;
	int originalNumber;

	m >> answer >> originalNumber;

	if (originalNumber * 3 == answer) {
		acceptClient(m.getHeader().senderID);
	}
	else
	{
		rejectClient(m.getHeader().senderID);
	}
}

void Server::rejectClient(int clientID) {
	for (auto& con : _connections) {
		if (con->getID() == clientID) {
			con->close();
			break;
		}
	}
}

void Server::acceptClient(int clientID) {
	for (auto& con : _connections) {
		if (con->getID() == clientID) {
			con->setVerified(true);

			Message accept;
			accept.getHeader().mesageType = MessageType::ACCEPT;

			con->send(accept);

			shareMessageLog(con);


			break;
		}
	}
}

void Server::messageAll(Message& m, int clientID = 0) {
	for (auto& con : _connections) {
		if (con->isOpen() && con->getID() != clientID)
		{
			con->send(m);
		}
	}
}


void Server::start(std::string& rawIp, unsigned short portNum) {
	boost::asio::ip::tcp::endpoint listenEP;
	boost::asio::ip::address ipAddress = boost::asio::ip::address::from_string(rawIp);

	listenEP.address(ipAddress);
	listenEP.port(portNum);
	_acceptor = new boost::asio::ip::tcp::acceptor(_context, listenEP.protocol());
	_acceptor->bind(listenEP);
	_acceptor->listen();

	acceptConnections();
}

void Server::update() {
	_inQueue.wait();

	while (!_inQueue.empty()) {
		Message m = _inQueue.pop_front();

		onMessage(m);
	}
}

void Server::addMessageToLog(Message& m) {
	_messageLog.push_back(m);
}

void Server::onMessage(Message& m) {
	if (m.getHeader().mesageType == MessageType::TEXT) {
		messageAll(m, m.getHeader().senderID);
		addMessageToLog(m);
		_textCtrl->AppendString(Message::extractTextMessage(m));
	}
	else if (m.getHeader().mesageType == MessageType::VERIFY_WITH_SERVER) {
		verifyClient(m);
	}
	else if (m.getHeader().mesageType == MessageType::UNAUTHORIZED) {
		std::cout << "Unauthorized device tried to send a message" << std::endl;
	}
}