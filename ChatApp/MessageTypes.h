#pragma once
enum class MessageType {
	TEXT = 1,
	REJECT,
	ACCEPT,
	VERIFY_CLIENT,
	VERIFY_WITH_SERVER,
	UNAUTHORIZED,
	KICK
};